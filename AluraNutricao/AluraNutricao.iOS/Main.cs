﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using br.com.alura.aluranutricao.Config;

namespace AluraNutricao.iOS
{
    public class Application
    {
        // This is the main entry point of the application.
        static void Main(string[] args)
        {

            Config.PathApp = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            // if you want to use a different Application Delegate class from "AppDelegate"
            // you can specify it here.
            UIApplication.Main(args, null, "AppDelegate");
        }
    }
}
