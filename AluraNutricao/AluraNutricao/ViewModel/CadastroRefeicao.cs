﻿using br.com.alura.aluranutricao.Data;
using br.com.alura.aluranutricao.Model;
using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace br.com.alura.aluranutricao.ViewModel
{
    public class CadastroRefeicao : INotifyPropertyChanged
    {

        private string descricao;
        private double calorias;
        private RefeicaoDAO dao;
        private ContentPage page;

        public ICommand SalvaRefeicao { get; protected set; }

        public event PropertyChangedEventHandler PropertyChanged;



        public string Descricao
        {
            get
            {
                return descricao;
            }
            set
            {
                if (value != descricao)
                {
                    descricao = value;
                    OnPropertyChanged("Descricao");
                }
            }
        }

        public double Calorias
        {
            get
            {
                return calorias;
            }
            set
            {
                if (calorias != value)
                {
                    calorias = value;
                    OnPropertyChanged("Calorias");
                }
            }
        }

        private void OnPropertyChanged(string nome)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(nome));
        }


        public CadastroRefeicao(RefeicaoDAO DAO, ContentPage pag)
        {
            this.dao = DAO;
            this.page = pag;

            SalvaRefeicao = new Command(() => {
                Refeicao refeicao = new Refeicao(descricao, calorias);
                dao.Salvar(refeicao);

                string msg = "A refeição " + descricao + " de " + calorias + " calorias foi salva com sucesso";
                this.page.DisplayAlert("Salvar refeição", msg, "Ok");
            });


        }

        
    }
}
