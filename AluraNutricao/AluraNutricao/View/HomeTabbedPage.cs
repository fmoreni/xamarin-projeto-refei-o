﻿using br.com.alura.aluranutricao.Data;
using System.Collections.ObjectModel;
using SQLite;

using Xamarin.Forms;
using br.com.alura.aluranutricao.Model;

namespace br.com.alura.aluranutricao
{
    public class HomeTabbedPage : TabbedPage
    {
        public HomeTabbedPage()
        {

            SQLiteConnection con = DependencyService.Get<ISQLLite>().GetConnection();

           
            RefeicaoDAO DAO = new RefeicaoDAO(con);
            ObservableCollection<Refeicao> Refeicoes = DAO.GetAll();


            this.Children.Add(new MainPage(Refeicoes, DAO));
            this.Children.Add(new ListaRefeicoes(Refeicoes));
        }
    }
}
