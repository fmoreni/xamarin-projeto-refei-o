﻿using br.com.alura.aluranutricao.Model;
using SQLite;
using System;
using System.Collections.ObjectModel;

using Xamarin.Forms;

namespace br.com.alura.aluranutricao
{
    public partial class ListaRefeicoes : ContentPage
    {

        public ObservableCollection<Refeicao> Refeicoes { get; set; } 
        public String Nome { get; set; }
        
        public ListaRefeicoes(ObservableCollection<Refeicao> refe)
        {
            BindingContext = this;

            Refeicoes = refe;
            Nome = "Alura";

            InitializeComponent();
        }


        public async void AcaoItem(object sender, ItemTappedEventArgs e)
        {
            Refeicao refeSelecionada = e.Item as Refeicao;

            var resposta = await DisplayAlert("Remove Item", "Você tem certeza que deseja remover a refeição " + refeSelecionada.Descricao, "Sim", "Não");

            if (resposta) {

                SQLiteConnection con = DependencyService.Get<Data.ISQLLite>().GetConnection();
                Data.RefeicaoDAO DAO = new Data.RefeicaoDAO(con);
                DAO.Remover(refeSelecionada);
                Refeicoes.Remove(refeSelecionada);

                await DisplayAlert("Item", "Refeição removida com sucesso", "OK");
            }

        }
    }
}
