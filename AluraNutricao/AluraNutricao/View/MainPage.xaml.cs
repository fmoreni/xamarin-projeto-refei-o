﻿using br.com.alura.aluranutricao.Data;
using br.com.alura.aluranutricao.Model;
using br.com.alura.aluranutricao.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace br.com.alura.aluranutricao
{
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<Refeicao> Refeicoes { get; set; }
        public RefeicaoDAO DAO;

        

        public MainPage(ObservableCollection<Refeicao> refeicoes, RefeicaoDAO dao)
        {
            CadastroRefeicao vm = new CadastroRefeicao(dao, this);
            BindingContext = vm;
            InitializeComponent();

            Refeicoes = refeicoes;
            DAO = dao;
        }

        public void AtualizaContador(Object o, EventArgs e)
        {
            double valor = StepperCalorias.Value;
            LabelCalorias.Text = valor.ToString();
        }


        public void MostrarLista(Object sender, EventArgs e)
        {
            ListaRefeicoes tela = new ListaRefeicoes(Refeicoes);
            Navigation.PushAsync(tela);
        }

    }
}
