﻿using SQLite;
using System;

namespace br.com.alura.aluranutricao.Model
{
    public class Refeicao
    {
        public Refeicao(){
        }


        public Refeicao(string descricao, double caloria)
        {
            this.Descricao = descricao;
            this.Calorias = caloria;
        }

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String Descricao { get; set; }
        public double Calorias { get; set; }


    }
}
