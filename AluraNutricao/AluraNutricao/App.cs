﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace AluraNutricao
{
    public class App : Application
    {
        public App()
        {
            MainPage = new br.com.alura.aluranutricao.HomeTabbedPage();

            //MainPage = new NavigationPage( new br.com.alura.aluranutricao.MainPage() );
            /*
            // The root page of your application
            var content = new ContentPage
            {
                Title = "Alura Nutrição",
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        new Label {
                            //HorizontalTextAlignment = TextAlignment.Center,
                            XAlign = TextAlignment.Center,
                            Text = "Welcome to Xamarin Forms!"
                        }
                    }
                }
            };

            MainPage = new NavigationPage(content);
            */
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
