﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency( typeof(br.com.alura.aluranutricao.Data.SQLLiteAndroid) )]
namespace br.com.alura.aluranutricao.Data
{

    
    public class SQLLiteAndroid: ISQLLite
    {

        public SQLLiteAndroid()
        {

        }



        public SQLite.SQLiteConnection GetConnection()
        {
            var fileName = "Refeicoes.db3";

            var documents = Config.Config.PathApp;

            //var path = Path.Combine(documents, "..", "Library", fileName);
            var path = Path.Combine(documents, fileName);

            return new SQLite.SQLiteConnection(path);
        }

    }
}
