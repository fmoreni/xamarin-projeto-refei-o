﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace br.com.alura.aluranutricao.Data
{
    public interface ISQLLite
    {
        SQLite.SQLiteConnection GetConnection();
    }
}
