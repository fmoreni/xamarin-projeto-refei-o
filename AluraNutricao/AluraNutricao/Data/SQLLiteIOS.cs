﻿using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(br.com.alura.aluranutricao.Data.SQLLiteIOS))]
namespace br.com.alura.aluranutricao.Data
{
    public class SQLLiteIOS: ISQLLite
    {

        public SQLLiteIOS()
        {

        }

        public SQLite.SQLiteConnection GetConnection()
        {
            var fileName = "Refeicoes.db3";

            var documents = Config.Config.PathApp;

            var path = Path.Combine(documents, "..", "Library", fileName);

            return new SQLite.SQLiteConnection(path);
        }
    }
}
