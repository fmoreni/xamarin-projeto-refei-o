﻿using br.com.alura.aluranutricao.Model;
using System.Collections.ObjectModel;

namespace br.com.alura.aluranutricao.Data
{
    public class RefeicaoDAO
    {

        SQLite.SQLiteConnection conexao;

        public RefeicaoDAO( SQLite.SQLiteConnection conn )
        {
            conexao = conn;
            conexao.CreateTable<Refeicao>();
        }


        public void Salvar(Refeicao Refeicao)
        {
            conexao.Insert(Refeicao);
        }

        public void Remover(Refeicao Refeicao)
        {
            conexao.Delete<Refeicao>(Refeicao.Id);
        }


        public ObservableCollection<Refeicao> GetAll()
        {
            return new ObservableCollection<Refeicao>(conexao.Table<Refeicao>());
        }

    }
}
